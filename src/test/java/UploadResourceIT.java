
import java.io.FileInputStream;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.apache.cxf.jaxrs.client.WebClient;
import org.apache.cxf.jaxrs.ext.multipart.Attachment;
import org.apache.cxf.jaxrs.ext.multipart.ContentDisposition;
import org.apache.cxf.jaxrs.ext.multipart.MultipartBody;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import org.junit.Test;

public class UploadResourceIT {

    @Test
    public void testUploadFileTomeeCXF() throws Exception {

        try (FileInputStream stream = new FileInputStream("src/main/resources/teste.csv");) {
            WebClient webClient = WebClient
                    .create("http://localhost:8080/TestUploadRestEndpoint/resources/upload/multipart");
            webClient.encoding("UTF-8");
            webClient.type(MediaType.MULTIPART_FORM_DATA);
            ContentDisposition cd = new ContentDisposition(
                    "attachment;filename=teste.csv");
            Attachment att = new Attachment("file", stream, cd);
            Response response = webClient.post(new MultipartBody(att));
            
            assertThat(response.getStatus(),is(200));
            
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}

package br.com.speedcapital.testuploadrestendpoint;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;
import org.apache.cxf.jaxrs.ext.multipart.Attachment;
import org.apache.cxf.jaxrs.ext.multipart.ContentDisposition;
import org.apache.cxf.jaxrs.ext.multipart.MultipartBody;

/**
 *
 * @author rcpd2013
 */
@Path("upload")
public class UploadResource {

    @GET
    public Response get() {
        return Response.ok().build();
    }

    @POST
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    public Response teste1(InputStream is) {

        System.out.println("Nome: " + is.toString());

        return Response.ok().build();
    }

    @POST
    @Path("multipart")
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    public Response teste2(MultipartBody multipartBody) {

        Attachment att = multipartBody.getAttachment("file");

        MultivaluedMap<String, String> headers = att.getHeaders();
        System.out.println("Headers");
        headers.keySet().forEach(k -> {
            System.out.println("key: " + k);
            System.out.println("value: " + headers.get(k));
        });

        System.out.println("Content type: " + att.getContentType());
        ContentDisposition cd = att.getContentDisposition();

        Map<String, String> params = cd.getParameters();
        System.out.println("Parameters");
        params.keySet().forEach(k -> {
            System.out.println("key: " + k);
            System.out.println("value: " + params.get(k));
        });

        System.out.println("Type: " + cd.getType());

        try (BufferedReader br = new BufferedReader(new InputStreamReader(att.getDataHandler().getInputStream()))) {
            System.out.println("linha: " + br.readLine());
        } catch (IOException ex) {
            Logger.getLogger(UploadResource.class.getName()).log(Level.SEVERE, null, ex);
        }

        return Response.ok().build();
    }

}
